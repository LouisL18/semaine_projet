# coding: utf-8
import argparse
import random
import client
import const
import plateau
import case
import joueur
import math

# Les getters
def get_carac_jeu(carac_jeu):
    """Permet de récupérer toutes les caractéristiques du jeu dans un dictionnaire

    Complexité: O(n)

    Args:
        carac_jeu (str): la chaine qui décrit les caractéristiques du jeu

    Returns:
        dict: le dictionnaire qui regroupe les caractéristiques du jeu
    """
    carac = carac_jeu.split(';')
    return {'duree_act':int(carac[0]), 'duree_tot': int(carac[1]), 'reserve_init': int(carac[2]), 'duree_obj': int(carac[3]), 'penalite': int(carac[4]), 'bonus_touche': int(carac[5]), 'bonus_rechar': int(carac[6]), 'bonus_objet': int(carac[7])}

def get_co_objet(plat):
    """Permet de connaitre la position de tous les pistolets et bombes qui se trouvent sur le plateau

    Complexité: O(n²)

    Args:
        plat: le plateau
    
    Returns:
        set: l'ensemble des positions des pistolets et bombes du plateau
    """
    co = set()
    for ligne in range(plateau.get_nb_lignes(plat)):
        for colonne in range(plateau.get_nb_colonnes(plat)):
            if case.get_objet(plateau.get_case(plat, (ligne, colonne))) not in (const.AUCUN, const.BIDON, const.BOUCLIER):
                co.add((ligne, colonne))
    return co

def get_co_bidon(plat):
    """Permet de connaitre la position de tous les bidons ese trouvant sur le plateau

    Complexité: O(n²)

    Args:
        plat: le plateau

    Returns:
        set: l'ensemble des positions des bidons du plateau
    """
    coo = set()
    for ligne in range(plateau.get_nb_lignes(plat)):
        for colonne in range(plateau.get_nb_colonnes(plat)):
            if case.get_objet(plateau.get_case(plat, (ligne, colonne))) == const.BIDON:
                coo.add((ligne, colonne))
    return coo

def get_mes_cases(plat, ma_couleur, my_player):
    """Permet de connaitre la position de toutes nos cases sur le plateau

    Complexité: O(n³)

    Args:
        plat: le plateau
        ma_couleur (str): la chaine décrivant notre couleur
        

    Returns:
        set: l'ensemble des positions de nos cases sur le plateau
    """
    cases = set()
    for ligne in range(plateau.get_nb_lignes(plat)):
        for colonne in range(plateau.get_nb_colonnes(plat)):
            if case.get_couleur(plateau.get_case(plat, (ligne, colonne))) == ma_couleur and (ligne, colonne)!=joueur.get_pos(my_player):
                for v in voisins(plat, (ligne, colonne)):
                    if case.get_couleur(plateau.get_case(plat, v)) == ma_couleur:
                        cases.add((ligne, colonne))
    return cases

def get_cases_ennemi(plat, ma_couleur, my_player):
    """Permet de connaitre la position de toutes les cases de nos adversaires sur le plateau

    Complexité: O(n²)

    Args:
        plat: le plateau
        ma_couleur (str): la chaine décrivant notre couleur
        my_player: notre joueur 

    Returns:
        set: l'ensemble des positions des cases adverses sur le plateau
    """
    cases = set()
    for ligne in range(plateau.get_nb_lignes(plat)):
        for colonne in range(plateau.get_nb_colonnes(plat)):
            if ' ' != case.get_couleur(plateau.get_case(plat,(ligne,colonne))) != ma_couleur and (ligne,colonne) != joueur.get_pos(my_player):
                cases.add((ligne,colonne))
    return cases

def get_cases_vide(plat,my_player):
    """Permet de connaitre la position de toutes les cases vides sur le plateau

    Complexité: O(n²)

    Args:
        plat: le plateau
        my_player: notre joueur

    Returns:
        set: l'ensemble des positions des cases vides sur le plateau
    """
    cases = set()
    for ligne in range(plateau.get_nb_lignes(plat)):
        for colonne in range(plateau.get_nb_colonnes(plat)):
            if case.get_couleur(plateau.get_case(plat,(ligne,colonne))) == ' ' and (ligne,colonne)!=joueur.get_pos(my_player):
                if not case.est_mur(plateau.get_case(plat,(ligne,colonne))):
                    cases.add((ligne,colonne))
    return cases

# Fonction concernant la fabrcation du chemin le plus court
def est_sur_plat(plat, position):
    """Indique si la position est bien sur le plateau

    Complexité: O(1)

    Args:
        plat: le plateau
        position (tuple): un tuple de deux entiers de la forme (ligne, colonne) 

    Returns:
        bool: True si la position est bien sur le plateau et False sinon
    """
    return 0 <= position[0] <= plateau.get_nb_lignes(plat)-1 and 0 <= position[1] <= plateau.get_nb_colonnes(plat)-1

def voisins(plat, position):
    """Renvoie l'ensemble des positions voisines accessibles de la position renseignées
       Une case accessible est une case qui est sur le plateau et qui n'est pas un mur

    Complexité: O(n)

    Args:
        plat: le plateau
        position (tuple): un tuple de deux entiers de la forme (ligne, colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    position_voisins = set()
    (ligne, colonne) = position
    for pos in plateau.INC_DIRECTION.values():
        new_pos = (new_l, new_l) = (ligne+pos[0], colonne+pos[1])
        if est_sur_plat(plat, new_pos) and not case.est_mur(plateau.get_case(plat, new_pos)):
            position_voisins.add(new_pos)
    position_voisins.remove(position)
    return position_voisins

def fabrique_le_calque(plat, position_depart):
    """Permet de fabriquer le calque d'un plateau en utilisation le principe de l'inondation

    Complexité: O(n²)
       
    Args:
        plat: le plateau
        position_depart (tuple): un tuple de deux entiers de la forme (ligne, colonne) 

    Returns:
        list: une liste de listes représentant le calque du plateau avec pour chaque position la distance depuis le point de départ
    """
    calque = [[None for _ in range(plateau.get_nb_colonnes(plat))] for _ in range(plateau.get_nb_lignes(plat))]
    l_dep, c_dep = position_depart
    calque[l_dep][c_dep] = 0
    liste_case = [position_depart]
    indice = 0 
    while indice < len(liste_case):
        (ligne_case, colonne_case) = liste_case[indice]
        val_case = calque[ligne_case][colonne_case]
        for voisin in voisins(plat, liste_case[indice]):
            (ligne_voisin, colonne_voisin) = voisin
            val_voisin = calque[ligne_voisin][colonne_voisin]
            if val_voisin is None or val_voisin > val_case+1:
                calque[ligne_voisin][colonne_voisin] = val_case+1
            if voisin not in liste_case:
                liste_case.append(voisin)
        indice += 1
    return calque

def fabrique_chemin(plat, position_depart, position_arrivee):
    """Renvoie le plus court chemin entre position_depart position_arrivee

    Complexité: O(n²)

    Args:
        plat: le plateau
        position_depart (tuple): un tuple de deux entiers de la forme (ligne, colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (ligne, colonne) 

    Returns:
        list: une liste de positions entre position_arrivee (inclu) et position_depart (exclu)
        qui représente le plus court chemin entre les deux positions
    """
    calque = fabrique_le_calque(plat, position_depart)
    chemin = [position_arrivee]
    indice = 0
    try:
        while chemin[-1] != position_depart:
            (ligne_case, colonne_case) = chemin[indice]
            val_case = calque[ligne_case][colonne_case]
            for voisin in voisins(plat, chemin[indice]):
                (ligne_voisin, colonne_voisin) = voisin
                val_voisin = calque[ligne_voisin][colonne_voisin]
                if val_voisin < val_case and voisin not in chemin:
                    chemin.append(voisin)
            indice += 1
    except:
        return [position_depart]
    return chemin[:-1]


# Fonction permettant d'aider l'IA à prendre des décisions sur ses déplcements où sur ses tires
def calcul_cout_chemin(plat, chemin, ma_couleur, carac):
    """Permet de calculer le "cout" d'un chemin, c'est-à-dire la réserve dépensé pour aller jusqu'à la position d'arrivée

    Complexité: O(n)

    Args:
        plat: le plateau
        chemin (list): la liste des positions représentant un chemin
        ma_couleur (str): la chaine représentant notre couleur
        carac (dict): le dictionnaire nous donnant les caractéristiques du jeu

    Returns:
        int: le cout du chemin
    """
    cout = 0
    for pos in chemin:
        couleur = case.get_couleur(plateau.get_case(plat, pos))
        if couleur != ma_couleur:
            cout += carac['penalite']
        elif couleur == ma_couleur:
            cout -= carac['bonus_rechar']
    return cout

def plus_proche(position,liste_pos):
    """Permet de donner la case avec la distance la plus faible entre une position et une liste de position

    Complexité: O(n)

    Args:
        position (tuple): le tuple de la forme (ligne, colonne) représentant la position de notre joueur
        liste_pos (liste): liste tuple de la forme (ligne, colonne) représentant des positions

    Returns:
        tuple: le tuple de la forme (ligne, colonne) représentant la position la plus proche de la position du joueur dans une liste de positions
    """
    res = None
    mini = None
    (x1,y1) = position
    for x2,y2 in liste_pos:
        if res is None or mini>abs(x1-x2) + abs(y1-y2):
            res = (x2,y2)
            mini = abs(x1-x2) + abs(y1-y2)
    return res


def direction(position1,position2):
    """Donne la direction de la position2 par rapport à la position1

    Complexité: O(1)

    Args:
        position1 (tuple): le tuple de la forme (ligne, colonne) représentant une position
        position2 (tuple): le tuple de la forme (ligne, colonne) représentant une position 

    Returns:
        str: la direction de la position 2 par rapport à la position1
    """
    (x,y)=position1
    if position2 == (x+1,y):
        return 'S'
    if position2 == (x-1,y):
        return 'N'
    if position2 == (x,y+1):
        return 'E'
    if position2 == (x,y-1):
        return 'O'
    return 'X'



# Gestion des déplacements
def move_ennemi(plat, ma_couleur, my_player):
    """Permet de savoir quelle direction prendre pour se déplacer vers une case adverse et s'il y a un problème, 
    aller dans une direction aléatoire parmi les directions possibles depuis notre position

    Complexité: O(n²)

    Args:
        plat: le plateau
        ma_couleur (str): la chaine représentant notre couleur
        my_player: notre joueur

    Returns:
        str: la direction vers laquelle se déplacer
    """
    co_proche_ennemi = plus_proche(joueur.get_pos(my_player), get_cases_ennemi(plat, ma_couleur, my_player))
    chemin = fabrique_chemin(plat, joueur.get_pos(my_player), co_proche_ennemi)
    if co_proche_ennemi != None and len(chemin)!=0:
        return direction(joueur.get_pos(my_player), chemin[-1])
    return random.choice(list(plateau.directions_possibles(plat, joueur.get_pos(my_player))))

def move_vide(plat, ma_couleur, my_player, carac):
    """Permet de savoir quelle direction prendre pour se déplacer vers une case vide et s'il y a un problème, 
    aller dans la direction d'une case adverse

    Complexité: O(n²)

    Args:
        plat: le plateau
        ma_couleur (str): la chaine représentant notre couleur
        my_player: notre joueur
        carac (dict): le dictionnaire nous donnant les caractéristiques du jeu

    Returns:
        str: la direction vers laquelle se déplacer
    """
    co_proche_vide = plus_proche(joueur.get_pos(my_player), get_cases_vide(plat, my_player))
    chemin = fabrique_chemin(plat, joueur.get_pos(my_player), co_proche_vide)
    if co_proche_vide != None and len(chemin)!=0:
        if calcul_cout_chemin(plat, chemin, ma_couleur, carac) < joueur.get_reserve(my_player)-5:
            return direction(joueur.get_pos(my_player), chemin[-1])
    return move_ennemi(plat, ma_couleur, my_player)

def move_recharge(plat, ma_couleur, my_player):
    """Permet de savoir quelle direction prendre pour se déplacer vers une de nos cases et s'il y a un problème, 
    aller dans une direction aléatoire parmi les directions possibles depuis notre position

    Complexité: O(n³)

    Args:
        plat: le plateau
        ma_couleur (str): la chaine représentant notre couleur
        my_player: notre joueur

    Returns:
        str: la direction vers laquelle se déplacer
    """
    if joueur.get_reserve(my_player) < 1 and joueur.get_surface(my_player) <= 1:
        co_bidon_proche = plus_proche(joueur.get_pos(my_player), get_co_bidon(plat))
        chemin = fabrique_chemin(plat, joueur.get_pos(my_player), co_bidon_proche)
        if co_bidon_proche != None and len(chemin)!=0:
            return direction(joueur.get_pos(my_player), chemin[-1])
    co_proche = plus_proche(joueur.get_pos(my_player), get_mes_cases(plat, ma_couleur, my_player))
    if co_proche != None: return direction(joueur.get_pos(my_player), fabrique_chemin(plat, joueur.get_pos(my_player), co_proche)[-1])
    return random.choice(list(plateau.directions_possibles(plat, joueur.get_pos(my_player))))



# Gestion du tire
def bien_tirer(plat, my_player, carac):
    """Permet de savoir dans quelle direction tirer pour avoir le meilleur ratio (point/cout)

    Complexité: O(n)

    Args:
        plat: le plateau
        my_player: notre joueur
        carac (dict): le dictionnaire nous donnant les caractéristiques du jeu

    Returns:
        str: la meilleure direction pour tirer
    """
    ration_score_par_dir = {}
    def get_ratio(dir):
        return ration_score_par_dir[dir]
    for dir in plateau.INC_DIRECTION:
        info = plateau.peindre(plat.copy(), joueur.get_pos(my_player), dir, joueur.get_couleur(my_player), joueur.get_reserve(my_player), const.DIST_MAX, joueur.get_objet(my_player)==const.PISTOLET)
        if (info['cout']+(len(info['joueurs_touches'])*carac['bonus_touche'])) == 0:
            ration_score_par_dir[dir] = (info['nb_repeintes']+info['nb_murs_repeints'])
        else:
            ration_score_par_dir[dir] = (info['nb_repeintes']+info['nb_murs_repeints'])/(info['cout']+(len(info['joueurs_touches'])*carac['bonus_touche']))
        
        if ration_score_par_dir[dir] == 0:
            ration_score_par_dir.pop(dir)
    if len(ration_score_par_dir) == 0 or joueur.get_reserve(my_player) < 2:
        return 'X'
    return max(ration_score_par_dir, key=get_ratio)


# L'IA
def mon_IA(ma_couleur,carac_jeu, plan, les_joueurs):
    """ Cette fonction permet de calculer les deux actions du joueur de couleur ma_couleur
        en fonction de l'état du jeu décrit par les paramètres. 
        Le premier caractère est parmi XSNOE X indique pas de peinture et les autres
        caractères indique la direction où peindre (Nord, Sud, Est ou Ouest)
        Le deuxième caractère est parmi SNOE indiquant la direction où se déplacer.

    Complexité: O(n³)

    Args:
        ma_couleur (str): un caractère en majuscule indiquant la couleur du jeur
        carac_jeu (str): une chaine de caractères contenant les caractéristiques
                                   de la partie séparées par des ;
             duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet           
        plan (str): le plan du plateau comme comme indiqué dans le sujet
        les_joueurs (str): le liste des joueurs avec leur caractéristique (1 joueur par ligne)
        couleur;reserve;nb_cases_peintes;objet;duree_objet;ligne;colonne;nom_complet
    
    Returns:
        str: une chaine de deux caractères en majuscules indiquant la direction de peinture
            et la direction de déplacement
    """
    #Variables du jeu
    plat = plateau.Plateau(plan)
    carac = get_carac_jeu(carac_jeu)
    player = [joueur.joueur_from_str(p) for p in les_joueurs.split('\n')]
    players = {joueur.get_couleur(p) : p for p in player}
    my_player = players[ma_couleur]

    #Coordonnées des bombes et pistolets
    co_objet = get_co_objet(plat)

    #Implémentation de la stratégie
    if joueur.get_reserve(my_player) < 3: #Gestion de la réserve
        movement = move_recharge(plat, ma_couleur,my_player)
    elif len(co_objet) != 0 and joueur.get_objet(my_player) == const.AUCUN: #Gestion des objets
        chemin = fabrique_chemin(plat, joueur.get_pos(my_player), plus_proche(joueur.get_pos(my_player), co_objet))
        if calcul_cout_chemin(plat, chemin, ma_couleur, carac) < joueur.get_reserve(my_player)-2 and len(chemin)!=0:
            movement=direction(joueur.get_pos(my_player), chemin[-1])
        else: 
            movement = move_vide(plat, ma_couleur, my_player, carac)
    elif len(get_cases_vide(plat, my_player)) > 0: #Gestion des déplacements
        movement = move_vide(plat, ma_couleur, my_player, carac)
    else:
        movement = move_ennemi(plat, ma_couleur, my_player)

    return bien_tirer(plat, my_player, carac)+movement


if __name__=="__main__":
    parser = argparse.ArgumentParser()  
    parser.add_argument("--equipe", dest="nom_equipe", help="nom de l'équipe", type=str, default='Non fournie')
    parser.add_argument("--serveur", dest="serveur", help="serveur de jeu", type=str, default='localhost')
    parser.add_argument("--port", dest="port", help="port de connexion", type=int, default=1111)
    
    args = parser.parse_args()
    le_client=client.ClientCyber()
    le_client.creer_socket(args.serveur,args.port)
    le_client.enregistrement(args.nom_equipe,"joueur")
    ok=True
    while ok:
        ok,id_joueur,le_jeu=le_client.prochaine_commande()
        if ok:
            carac_jeu,plat,les_joueurs=le_jeu.split("--------------------\n")
            actions_joueur=mon_IA(id_joueur,carac_jeu,plat,les_joueurs[:-1])
            le_client.envoyer_commande_client(actions_joueur)
            # le_client.afficher_msg("sa reponse  envoyée "+str(id_joueur)+args.nom_equipe)
    le_client.afficher_msg("terminé")
