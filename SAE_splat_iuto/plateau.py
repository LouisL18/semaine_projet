"""module de gestion du plateau de jeu
"""
import const
import case


# dictionnaire permettant d'associer une direction et la position relative
# de la case qui se trouve dans cette direction
INC_DIRECTION = {'N': (-1, 0), 'E': (0, 1), 'S': (1, 0),
                 'O': (0, -1), 'X': (0, 0)}


def get_nb_lignes(plateau):
    """retourne le nombre de lignes du plateau
    Args: plateau (dict): le plateau considéré
    Returns: int: le nombre de lignes du plateau
    """
    def get_ligne(co):
        return co[0]
    return max(plateau, key=get_ligne)[0]+1

def get_nb_colonnes(plateau):
    """retourne le nombre de colonnes du plateau
    Args: plateau (dict): le plateau considéré
    Returns: int: le nombre de colonnes du plateau
    """
    def get_col(co):
        return co[1]
    return max(plateau, key=get_col)[1]+1

def get_case(plateau, pos):
    """retourne la case qui se trouve à la position pos du plateau
    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int
    Returns: dict: La case qui se situe à la position pos du plateau
    """
    (line, col) = pos
    if 0<=line<=get_nb_lignes(plateau) and 0<=col<=get_nb_colonnes(plateau):
        return plateau[(line, col)]

def set_case(plateau, pos, une_case):
    """remplace la case qui se trouve en position pos du plateau par une_case
    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int
        une_case (dict): la nouvelle case
    """
    (line,col) = pos
    plateau[(line, col)] = une_case

def poser_joueur(plateau, joueur, pos):
    """pose un joueur en position pos sur le plateau
    Args:
        plateau (dict): le plateau considéré
        joueur (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int
    """
    (line, col) = pos
    if 0<=line<=get_nb_lignes(plateau) and 0<=col<=get_nb_colonnes(plateau):
        box = get_case(plateau,pos)
        case.poser_joueur(box,joueur)

def poser_objet(plateau, objet, pos):
    """Pose un objet en position pos sur le plateau. Si cette case contenait déjà
        un objet ce dernier disparait

    Args:
        plateau (dict): le plateau considéré
        objet (int): un entier représentant l'objet. const.AUCUN indique aucun objet
        pos (tuple): une paire (lig,col) de deux int
    """
    (line, col) = pos
    if 0<=line<=get_nb_lignes(plateau) and 0<=col<=get_nb_colonnes(plateau):
        box = get_case(plateau,pos)
        case.poser_objet(box,objet)
    

def plateau_from_str(file):
    """Construit un plateau à partir d'une chaine de caractère contenant les informations
        sur le contenu du plateau (voir sujet)
    Args: la_chaine (str): la chaine de caractères décrivant le plateau
    Returns: list: le plateau correspondant à la chaine (sous forme de ses lignes).
    None si l'opération a échoué
    """
    with open(file) as fic:
        return fic.read()

def Plateau(plan):
    """Créer un plateau en respectant le plan donné en paramètre.
        Le plan est une chaine de caractères contenant
            '#' (mur)
            ' ' (couloir non peint)
            une lettre majuscule (un couloir peint par le joueur représenté par la lettre)
    Args: plan (str): le plan sous la forme d'une chaine de caractères
    Returns: list: Le plateau correspondant au plan
    """
    plan_split = plan.split('\n')
    lignes, colonnes = plan_split.pop(0).split(';')
    plateau = {}
    nombre_joueur = int(plan_split[int(lignes)])
    nombre_objet = int(plan_split[int(lignes)+nombre_joueur+1])
    for ligne in range(int(lignes)):
        for colonne in range(int(colonnes)):
            box = plan_split[ligne][colonne]
            if box == ' ':
                plateau[(ligne, colonne)] = case.Case()
            elif box.isalpha():
                if box.isupper(): plateau[(ligne, colonne)] = case.Case(couleur=box)
                else: plateau[(ligne, colonne)] = case.Case(True, box)
            else: plateau[(ligne, colonne)] = case.Case(True)
    for j in range(nombre_joueur):
        joueur, l, c = plan_split[int(lignes)+j+1].split(';')
        poser_joueur(plateau,joueur,(int(l),int(c)))
    for o in range(nombre_objet):
        objet, l, c = plan_split[int(lignes)+nombre_joueur+o+2].split(';')
        poser_objet(plateau,int(objet),(int(l),int(c)))

    return plateau


def display(plateau):
    for line in range(get_nb_lignes(plateau)):
        print()
        for colonne in range(get_nb_colonnes(plateau)): print(plateau[(line, colonne)])


plan = Plateau(plateau_from_str('plans/plan1.txt'))
# print(plan)
# poser_joueur(plan,'Z',(3,5))
# print('Z' in case.get_joueurs(get_case(plan,(3,5))))

def enlever_joueur(plateau, joueur, pos):
    """enlève un joueur qui se trouve en position pos sur le plateau
    Args:
        plateau (dict): le plateau considéré
        joueur (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int
    Returns: bool: True si l'opération s'est bien déroulée, False sinon
    """
    (line, col) = pos
    if 0<=line<=get_nb_lignes(plateau) and 0<=col<=get_nb_colonnes(plateau):
        box = get_case(plateau,pos)
        return case.prendre_joueur(box,joueur)

def prendre_objet(plateau, pos):
    """Prend l'objet qui se trouve en position pos du plateau et retourne l'entier
        représentant cet objet. const.AUCUN indique qu'aucun objet se trouve sur case
    Args:
        plateau (dict): Le plateau considéré
        pos (tuple): une paire (lig,col) de deux int
    Returns:
        int: l'entier représentant l'objet qui se trouvait sur la case.
        const.AUCUN indique aucun objet
    """
    (line, col) = pos
    if 0<=line<=get_nb_lignes(plateau) and 0<=col<=get_nb_colonnes(plateau):
        return case.prendre_objet(get_case(plateau,pos))

def deplacer_joueur(plateau, joueur, pos, direction):
    """Déplace dans la direction indiquée un joueur se trouvant en position pos
        sur le plateau

    Args:
        plateau (dict): Le plateau considéré
        joueur (str): La lettre identifiant le joueur à déplacer
        pos (tuple): une paire (lig,col) d'int
        direction (str): une lettre parmie NSEO indiquant la direction du déplacement

    Returns:
        tuple: un tuple contenant 4 informations
            - un bool indiquant si le déplacement a pu se faire ou non
            - un int valeur une des 3 valeurs suivantes:
                *  1 la case d'arrivée est de la couleur du joueur
                *  0 la case d'arrivée n'est pas peinte
                * -1 la case d'arrivée est d'une couleur autre que celle du joueur
            - un int indiquant si un objet se trouvait sur la case d'arrivée (dans ce
                cas l'objet est pris de la case d'arrivée)
            - une paire (lig,col) indiquant la position d'arrivée du joueur (None si
                le joueur n'a pas pu se déplacer)
    """
    (line, col) = pos
    if 0<=line<=get_nb_lignes(plateau) and 0<=col<=get_nb_colonnes(plateau):
        box = get_case(plateau,pos)
        if joueur not in case.get_joueurs(box): return (False,0,const.AUCUN,pos)
        newline,new_col = INC_DIRECTION[direction]
        new_pos = (line+newline,col+new_col)
        new_box = get_case(plateau,new_pos)
        if case.est_mur(new_box): #Si déplacement pas abouti
            if case.get_couleur(box) == ' ': return (False,0,const.AUCUN,pos) #si sa case contient aucune couleur
            elif case.get_couleur(box) == joueur: return (False,1,const.AUCUN,pos) #si sa case contient sa propre couleur
            else: return (False,-1,const.AUCUN,pos) #si sa case contient la couleur d'un autre joueur
        else:
            objet = case.get_objet(new_box)
            if objet != const.AUCUN: prendre_objet(plateau,new_pos)
            case.poser_joueur(new_box,joueur) #Nouvelle position du joueur
            case.prendre_joueur(box,joueur) #Retirer son ancienne position
            if case.get_couleur(new_box) == ' ': return (True,0,objet,new_pos)
            elif case.get_couleur(new_box) == joueur: return (True,1,objet,new_pos)
            else: return (True,-1,objet,new_pos)

#-----------------------------
# fonctions d'observation du plateau
#-----------------------------

def surfaces_peintes(plateau, nb_joueurs):
    """retourne un dictionnaire indiquant le nombre de cases peintes pour chaque joueur.

    Args:
        plateau (dict): le plateau considéré
        nb_joueurs (int): le nombre de joueurs total participant à la partie

    Returns:
        dict: un dictionnaire dont les clées sont les identifiants joueurs et les
            valeurs le nombre de cases peintes par le joueur
    """
    frequences = dict()
    for ligne in range(get_nb_lignes(plateau)):
        for colonne in range(get_nb_colonnes(plateau)):
            for j in case.get_joueurs(get_case(plateau, (ligne, colonne))):
                frequences[j] = 0
    if nb_joueurs == len(frequences):
        for ligne in range(get_nb_lignes(plateau)):
            for colonne in range(get_nb_colonnes(plateau)):
                couleur = case.get_couleur(get_case(plateau, (ligne, colonne))).upper()
                if couleur != ' ':
                    frequences[couleur] += 1
    return frequences

def directions_possibles(plateau,pos):
    """ retourne les directions vers où il est possible de se déplacer à partir
        de la position pos
    Args:
        plateau (dict): le plateau considéré
        pos (tuple): un couple d'entiers (ligne,colonne) indiquant la position de départ
    Returns:
        dict: un dictionnaire dont les clés sont les directions possibles et les valeurs la couleur
              de la case d'arrivée si on prend cette direction
              à partir de pos
    """
    (line, col) = pos
    if 0<=line<get_nb_lignes(plateau) and 0<=col<get_nb_colonnes(plateau):
        directions = dict()
        box = get_case(plateau,pos)
        if not case.est_mur(box):
            for direction in INC_DIRECTION:
                new_line,new_col = INC_DIRECTION[direction]
                new_pos = (line+new_line,col+new_col)
                (line2, col2) = new_pos
                if 0<=line2<get_nb_lignes(plateau) and 0<=col2<get_nb_colonnes(plateau) and new_pos != pos:
                    new_box = get_case(plateau,new_pos)
                    if not case.est_mur(new_box):
                        directions[direction]=case.get_couleur(new_box)
        return directions
    
def nb_joueurs_direction(plateau, pos, direction, distance_max):
    """indique combien de joueurs se trouve à portée sans protection de mur.
        Attention! il faut compter les joueurs qui sont sur la case pos

    Args:
        plateau (dict): le plateau considéré
        pos (_type_): la position à partir de laquelle on fait le recherche
        direction (str): un caractère 'N','O','S','E' indiquant dans quelle direction on regarde
    Returns:
        int: le nombre de joueurs à portée de peinture (ou qui risque de nous peindre)
    """
    (line, col) = pos
    if 0<=line<get_nb_lignes(plateau) and 0<=col<get_nb_colonnes(plateau):
        nb_joueur = 0
        for i in range(distance_max):
            new_line,new_col = INC_DIRECTION[direction]
            new_pos = (line+new_line*i,col+new_col*i)
            (line2, col2) = new_pos
            if 0<=line2<get_nb_lignes(plateau) and 0<=col2<get_nb_colonnes(plateau):
                new_box = get_case(plateau,new_pos)
                if case.est_mur(new_box): return nb_joueur
                nb_joueur+= len(case.get_joueurs(new_box))
        return nb_joueur

    
def peindre(plateau, pos, direction, couleur, reserve, distance_max, peindre_murs=False):
    """ Peint avec la couleur les cases du plateau à partir de la position pos dans
        la direction indiquée en s'arrêtant au premier mur ou au bord du plateau ou
        lorsque que la distance maximum a été atteinte.
    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de int
        direction (str): un des caractères 'N','S','E','O' indiquant la direction de peinture
        couleur (str): une lettre indiquant l'idenfiant du joueur qui peint (couleur de la peinture)
        reserve (int): un entier indiquant la taille de la reserve de peinture du joueur
        distance_max (int): un entier indiquant la portée maximale du pistolet à peinture
        peindre_mur (bool): un booléen indiquant si on peint aussi les murs ou non
    Returns:
        dict: un dictionnaire avec 4 clés
                "cout": un entier indiquant le cout en unités de peinture de l'action
                "nb_repeintes": un entier indiquant le nombre de cases qui ont changé de couleur
                "nb_murs_repeints": un entier indiquant le nombre de murs qui ont changé de couleur
                "joueurs_touches": un ensemble (set) indiquant les joueurs touchés lors de l'action
    """
    (line, col) = pos
    if 0<=line<get_nb_lignes(plateau) and 0<=col<get_nb_colonnes(plateau):
        res = {'cout':0,'nb_repeintes':0,'nb_murs_repeints':0,'joueurs_touches':set()}
        i=0
        end = False
        for i in range(distance_max):
            new_line,new_col = INC_DIRECTION[direction]
            new_pos = (line+new_line*i,col+new_col*i)
            (line2, col2) = new_pos
            if 0<=line2<get_nb_lignes(plateau) and 0<=col2<get_nb_colonnes(plateau):
                new_box = get_case(plateau,new_pos)
                couleur_new_box = case.get_couleur(new_box)
                cout = 1 + (couleur_new_box!=couleur and couleur_new_box!=' ')
                if case.est_mur(new_box) and not peindre_murs: return res
                else:
                    if reserve >= cout:
                        case.peindre(new_box,couleur)
                        reserve -= cout
                        res['cout']+=cout
                        if case.est_mur(new_box): res['nb_murs_repeints']+=1
                        if couleur_new_box != couleur: res['nb_repeintes']+=1
                        res['joueurs_touches'] = res['joueurs_touches'].union(case.get_joueurs(new_box))
                    else: return res
        return res      